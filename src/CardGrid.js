import React from 'react';
import { Card, Col, Row } from 'antd';
import 'antd/dist/antd.css';

import { baseURL } from './constants';

class CardGrid extends React.Component {
  constructor() {
    super();
    this.state = {
      firstTime: 0,
      repeat: 0,
    };
  }

  componentDidMount() {
    fetch(`${baseURL}/user/one_time`)
      .then(res => {
        if (!res.ok) throw Error(res.statusText);
        return res.json();
      })
      .then(res => {
        this.setState({ firstTime: res.data });
      });

    fetch(`${baseURL}/user/repeat`)
      .then(res => {
        if (!res.ok) throw Error(res.statusText);
        return res.json();
      })
      .then(res => {
        this.setState({ repeat: res.data });
      });
  }
  render() {
    const { firstTime, repeat } = this.state;
    return (
      <div>
        <Row gutter={16} style={{ marginTop: '1em' }}>
          <Col span={12}>
            <Card
              title="First time"
              bodyStyle={{
                'fontSize': '5vw',
              }}
            >{firstTime.toLocaleString('en-IN')}</Card>
          </Col>
          <Col span={12}>
            <Card
              title="Repeat"
              bodyStyle={{
                'fontSize': '5vw',
              }}
            >{repeat.toLocaleString('en-IN')}</Card>
          </Col>
        </Row>
        <Row gutter={16} style={{ marginTop: '1em' }}>
          <Col span={12}>
            <Card
              title="Bounce rate"
              bodyStyle={{
                'fontSize': '5vw',
              }}
            >{(firstTime * 100 / (firstTime + repeat)).toFixed(2)}</Card>
          </Col>
          <Col span={12}>
            <Card
              title="% Visits from repeat users"
              bodyStyle={{
                'fontSize': '5vw',
              }}
            >{(100 - (firstTime * 100) / (firstTime + repeat)).toFixed(2)}</Card>
          </Col>
        </Row>
      </div>
    );
  }
};

export default CardGrid;
