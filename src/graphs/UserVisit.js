import React, { Component } from 'react';
import { ResponsiveContainer, BarChart, XAxis, YAxis, CartesianGrid, Tooltip, Legend, Brush, Bar } from 'recharts';
import { baseURL } from '../constants';

class UserVisit extends Component {
  constructor() {
    super();
    this.state = {
      rows: [],
    };
  }

  componentDidMount() {
    fetch(`${baseURL}/user/repeat_n`)
      .then(res => {
        if (!res.ok) throw Error(res.statusText);
        return res.json();
      })
      .then(res => {
        this.setState({ rows: res.data });
      });
  }

  render() {
    const { rows } = this.state;
    const { aspect } = this.props;
    if (rows.length === 0) {
      return <div></div>;
    }
    return (
      <div>
        <h2 style={{ textAlign: 'center', marginTop: 10 }}>Users who visited n times</h2>
        <ResponsiveContainer aspect={aspect} width='100%'>
          <BarChart data={rows}
            margin={{ top: 25, right: 40, left: 0, bottom: 5 }}>
            <XAxis label="Visits" dataKey="visits" />
            <YAxis label="Number of users" />
            <CartesianGrid strokeDasharray="3 3" />
            <Tooltip />
            <Legend verticalAlign="top" height={36} />
            <Brush dataKey="visits" height={30} stroke="#334961" />
            <Bar dataKey="count" fill="#334961"/>
          </BarChart>
        </ResponsiveContainer>
      </div>
    );
  }
}

export default UserVisit;
