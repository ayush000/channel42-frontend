import React, { Component } from 'react';
import { ResponsiveContainer, BarChart, XAxis, YAxis, CartesianGrid, Tooltip, Legend, Brush, Bar } from 'recharts';
import { baseURL } from '../constants';

class UserVisitMonthly extends Component {
  constructor() {
    super();
    this.state = {
      rows: [],
    };
  }

  componentDidMount() {
    fetch(`${baseURL}/user/recurring_visits_monthly`)
      .then(res => {
        if (!res.ok) throw Error(res.statusText);
        return res.json();
      })
      .then(res => {
        this.setState({ rows: res.data });
      });
  }

  render() {
    const { rows } = this.state;
    const { aspect } = this.props;
    if (rows.length === 0) {
      return <div></div>;
    }
    return (
      <div>
        <h2 style={{ textAlign: 'center', marginTop: 10 }}>Visits by repeat users</h2>
        <ResponsiveContainer aspect={aspect} width='100%'>
          <BarChart data={rows}
            margin={{ top: 25, right: 40, left: 0, bottom: 5 }}>
            <XAxis label="Month" dataKey="visit_month" />
            <YAxis label="Count" />
            <CartesianGrid strokeDasharray="3 3" />
            <Tooltip />
            <Legend verticalAlign="top" height={36} />
            <Brush dataKey="visit_month" height={30} stroke="#334961" />
            <Bar dataKey="visits" name="Total Visits" fill="#334961"/>
            <Bar dataKey="users" name="Number of Users" fill="#7d87b9"/>
          </BarChart>
        </ResponsiveContainer>
      </div>
    );
  }
}

export default UserVisitMonthly;
