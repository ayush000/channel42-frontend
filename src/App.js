import React, { Component } from 'react';
import './App.css';
import CardGrid from './CardGrid';
import UserVisit from './graphs/UserVisit';
import UserVisitMonthly from './graphs/UserVisitMonthly';
import BounceRateMonthly from './graphs/BounceRateMonthly';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="AppHeader">
          <h1>Channel42 Social Analysis</h1>
        </div>
        <div className="AppBody">
          <CardGrid />
          <UserVisit aspect={4.0 / 1.5}/>
          <UserVisitMonthly aspect={4.0 / 1.5}/>
          <BounceRateMonthly aspect={4.0 / 1.5} />
        </div>
      </div>
    );
  }
}

export default App;
