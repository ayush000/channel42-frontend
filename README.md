# Channel42 Analysis

* Install Node.js and npm

* `npm install` installs all the dependencies.

* PORT=4000 `npm start` starts the project in development mode.

* Use `npm run build` to create production build, and then host `index.html` created in build/ folder using static server